﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;


public class FileReader : MonoBehaviour
{
    [SerializeField] private string fileName;
    [SerializeField] private List<int> multiples;

    private List<string> numbersString;
    private List<int> numbersInt;

 
    private void Start ()
    {
        // Number Sorting 1 & 2
        LoadFile();

        // Number Sorting 3
        for (int i = 0; i < multiples.Count; i++)
            CheckMultiples(multiples[i]);

        // Number Sorting 4
        CheckPrime();
    }


    private void LoadFile()
    {
        numbersString = new List<string>();
        numbersInt = new List<int>();

        // Create FilePath
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        // Create StreamReader using filePath
        StreamReader sr = new StreamReader(filePath);

        // use SR to read the txt file and save to string
        string fileContents = sr.ReadToEnd();

        sr.Close();

        // Break down string into lines
        string[] arraystring = fileContents.Split("\n"[0]);

        // Loop through all lines
        for (int i = 0; i < arraystring.Length; i++)
        {
            int number;

            // check if line is an int
            if (int.TryParse(arraystring[i], out number))
            {
                numbersString.Add(arraystring[i]); // Save as String
                numbersInt.Add(number); // Save as Integer
            }

            // Error if not an int
            else
                Debug.Log("Error, Line: " + i + ", is invalid. Line contents: " + arraystring[i]);
        }

        // Sort List
        SortNumbers();

        // Print List
        PrintIntList(numbersInt);
    }


    private void SortNumbers()
    {
        // Sort list from smallest to highest
        numbersInt = numbersInt.OrderBy(numbersInt => numbersInt).ToList();

        // Could sort by largest First
        //numbersInt = numbersInt.OrderByDescending(numbersInt => numbersInt).ToList();
    }


    private void CheckMultiples(int _inputNo)
    {
        for (int i = 0; i < numbersInt.Count; i++)
        {
            // Check for remainder
            if (numbersInt[i] % _inputNo == 0)
            {
                // If there isnt a remainder, it must be a multiple
                Debug.Log(numbersInt[i] + " Is a Multiple of: " + _inputNo);
            }
        }
    }


    private void CheckPrime()
    {
        List<int> primeNumbers = new List<int>();

        // Loop through all numbers
        for (int i = 0; i < numbersInt.Count; i++)
        {
            bool prime = true;

            int x = 2;

            // check for remainder
            while (x <= numbersInt[i] / 2)
            {
                // If we dont have a remainder, its not a prime
                if (numbersInt[i] % x == 0)
                {
                    prime = false;
                    break;
                }

                x++;
            }

            // If prime add to list
            if (prime)
                primeNumbers.Add(numbersInt[i]);
        }

        // Print prime numbers
        PrintIntList(primeNumbers);
    }


    // Helper function to print List<int> contents
    private void PrintIntList(List<int> _list)
    {
        foreach (int value in _list)
            Debug.Log(value);
    }
}