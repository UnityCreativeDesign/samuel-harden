﻿using UnityEngine;
using System;


public class Palindromes : MonoBehaviour
{
    public string Input;

    
    // Palindromes 1, 2 & 3
	public void CheckInput() // (Public so EditorScript can access method)
    {
        int start = 0;
        int end = Input.Length - 1;

        while (start < end)
        {
            // if all chars have been checked, break
            if (start >= end)
                break;

            // Check chars are the same (case Insensitive)
            if (!string.Equals(Input[start].ToString(), Input[end].ToString(),
                StringComparison.CurrentCultureIgnoreCase))
            {
                Debug.Log("False, " + Input + " is NOT a Palindrome");
                return;
            }

            // update to next char index
            start++;
            end--;
        }

        // If all chars have been checked and we are here, it must be a Palindrome
        Debug.Log("True, " + Input + " is a Palindrome");
	}
}
