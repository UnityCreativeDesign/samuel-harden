﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(Palindromes))]
public class PalindromeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Palindromes palindromes = (Palindromes)target;

        if (GUILayout.Button("Check Input"))
        {
            palindromes.CheckInput();
        }
    }
}
