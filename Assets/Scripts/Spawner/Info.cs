﻿using UnityEngine;
using System;


[Serializable]
public class Info : MonoBehaviour 
{
    // Data
    [SerializeField] private string stringField;
    [SerializeField] private int intField;
    [SerializeField] private float floatField;
    [SerializeField] private bool boolField;


    public void SetInfo(information _info)
    {
        stringField = _info.stringField;
        intField    = _info.intField;
        floatField  = _info.floatField;
        boolField   = _info.boolField;
    }
}
