﻿using System;


// Stores the information from the Json file
[Serializable]
public class information
{
    // Data
    public string stringField;
    public int intField;
    public float floatField;
    public bool boolField;
}
