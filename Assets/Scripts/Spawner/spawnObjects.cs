﻿using System;
using UnityEngine;


// Stores the data from the Json file
[Serializable]
public class spawnObjects
{
    public string name;
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;
    public string primitive;
    public information information;
}
