﻿using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;


public class Spawner : MonoBehaviour
{
    // Wrapper Class
    [Serializable]
    public class SpawnObjectsList
    {
        public List<spawnObjects> spawnObjects;
    }


    [SerializeField] private string fileName;
    [SerializeField] private SpawnObjectsList objects;

    private List<GameObject> spawnedObjects;


	private void Start ()
    {
        objects = new SpawnObjectsList();

        spawnedObjects = new List<GameObject>();

        // Create FilePath
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        string jsonText = File.ReadAllText(filePath);

        // Split objects out from json
        objects = JsonUtility.FromJson<SpawnObjectsList>(jsonText);

        // Create Each Object from Json
        foreach (spawnObjects _object in objects.spawnObjects)
        {
            // Create & Store reference to GO
            spawnedObjects.Add(CreateGO(_object));
        }
	}


    private GameObject CreateGO(spawnObjects _object)
    {
        // Create the Object
        GameObject newObject = GameObject.CreatePrimitive(GetType(_object.primitive));

        // Set new Objects Name
        newObject.name = _object.name;

        // Set Transform data
        newObject.transform.position   = _object.position;
        newObject.transform.rotation   = Quaternion.Euler(_object.rotation);
        newObject.transform.localScale = _object.scale;

        // Add Information and Assign Data
        newObject.AddComponent<Info>();

        newObject.GetComponent<Info>().SetInfo(_object.information);

        return newObject;
    }


    // Which primitive do we want to Spawn?
    private PrimitiveType GetType(string _name)
    {
        switch (_name)
        {
            case "Cube":
                return PrimitiveType.Cube;
            case "Sphere":
                return PrimitiveType.Sphere;
            case "Capsule":
                return PrimitiveType.Capsule;
            case "Cylinder":
                return PrimitiveType.Cylinder;
            default:
                Debug.Log("Unknown Primitive: " + _name + " ...Defaulting to Cube");
                return PrimitiveType.Cube;
        }
    }
}
